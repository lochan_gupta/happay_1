# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Student(models.Model):
    #rollno=models.CharField(max_length=10,unique=True)
    first_name= models.CharField(max_length = 50)
    last_name = models.CharField(max_length = 50)
    rollno = models.CharField(max_length = 20,unique=True)
    college = models.CharField(max_length = 20)
    #mail = models.CharField(max_length = 22)
    mail = models.CharField(max_length = 22)
