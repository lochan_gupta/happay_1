# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import FieldDoesNotExist
from django.http import QueryDict

# Create your views here.
class TestSum(View):

        def get(self, request):
            try:
                p1 = request.GET.get('p1', '1')
                p2 = request.GET.get('p2', '1')
                result = int(p1) + int(p2)
                return HttpResponse('sum is : ' + str(result), status=200)
            except Exception as exception:
                print str(exception)
                return HttpResponse("Internal Server Error", status=500)

from internship.models import Student
class StudentData (View):
    def get(self, request):
     try:
         result = []
         students = Student.objects.all()
         for student in students:
            res = {}
            res['first_name'] = student.first_name
            res['last_name'] = student.last_name
            res['mail'] = student.mail
            res['rollno'] = student.rollno
            res['college'] = student.college
            result.append(res)
         return HttpResponse(result)
     except Exception as exception:
        return HttpResponse("Internal Server Error", status = 200)

class Insert(View):
  def post(self,request):
    try:
        first_name = request.POST["first_name"]
        last_name  = request.POST["last_name"]
        mail = request.POST["mail"]
        rollno = request.POST["rollno"]
        college = request.POST["college"]
        information = Student(first_name=first_name,last_name=last_name,mail=mail,rollno = rollno, college = college)
        information.save()
        return HttpResponse("successful", request)
    except Exception as IntegrityError:
        return HttpResponse("rollno already exists")
    except Exception as MultiValueDictKeyError:
        return HttpResponse("Multiple values for key in dictionary")

class Delete(View):
  def delete(self,request,id):
      try:
        #mail=request.POST["mail"]
          dil = Student.objects.get(id=id)
          dil.delete()
          return HttpResponse("successful!", request, status =200)
      except Exception as ObjectDoesNotExist:
          return HttpResponse("none field found!!")

class Updaterecord(View):
  def put(self, request,rollno):
     try:
        request = QueryDict(request.body)
        object = Student.objects.get(rollno = rollno)
        if 'first_name' in request.keys():
            object.first_name = request['first_name']
        if 'last_name' in request.keys():
            object.last_name = request['last_name']
        if 'mail' in request.keys():
            object.mail = request['mail']
        if 'college' in request.keys():
            object.college = request['college']

        object.save()
        return HttpResponse("rollnumber: " +str(rollno) + "successfully updated ", status= 200)
     except Exception as ObjectDoesNotExist:
        return HttpResponse ("object does not exit ", status = 500)
        #mail_id =request.POST["mail"]
        #college = request.POST["college"]
        #value = Student.objects.filter(mail = mail_id).update(college = college)

#        return HttpResponse(request)
