"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from internship.views import TestSum
from internship.views import StudentData
from internship.views import Insert
from internship.views import Delete
from internship.views import Updaterecord

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^Testsum/',TestSum.as_view(),name='h'),
    url(r'^Students/',StudentData.as_view(),name='s'),
    url(r'^Insert/', Insert.as_view(),name='r'),
    url(r'^Delete/(?P<id>\d+)',Delete.as_view(),name='d'),
    url(r'^Updaterecord/(?P<rollno>\d+)',Updaterecord.as_view(),name='cgf'),
]
